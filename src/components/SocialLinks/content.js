const links = [
  {
    label: "GitLab",
    url: "https://gitlab.com/oaraujocesar"
  },
  {
    label: "LinkedIn",
    url: "https://www.linkedin.com/in/cesaroaraujo/"
  },
  {
    label: "Instagram",
    url: "https://www.instagram.com/cesar0araujo"
  },
  {
    label: "Twitter",
    url: "https://twitter.com/Cesar0Araujo"
  },
  {
    label: "Facebook",
    url: "https://www.facebook.com/cesar.oliveira.33"
  },
]

export default links;