import { Gitlab as GitLab } from 'styled-icons/remix-fill/Gitlab'
import { Twitter } from 'styled-icons/icomoon/Twitter'
import { Linkedin as LinkedIn } from 'styled-icons/fa-brands/Linkedin'
import { Facebook } from 'styled-icons/icomoon/Facebook'
import { Instagram } from 'styled-icons/fa-brands/Instagram'


const Icons = {
  GitLab,
  LinkedIn,
  Twitter,
  Facebook,
  Instagram
}

export default Icons;